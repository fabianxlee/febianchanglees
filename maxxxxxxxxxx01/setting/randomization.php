<?php
$len = substr(str_shuffle("12345"), 0, 2);
function generateRandomString($length = 25)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789аЬсdеfghіјκlmnоρqгѕtuνwхуzАΒСDΕFGΗIЈΚLΜΝОРQRЅТUVWΧΥZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}
$myRandomString = generateRandomString($len);
$myRandomString2 = generateRandomString($len);
$myRandomString3 = generateRandomString($len);


?>
