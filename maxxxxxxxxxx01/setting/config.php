<?php

function checkTimeGetNumber($time, $different, $base64Number)
{
    if (isset($_GET['u'])) {
        $unixtime = substr($_GET['u'], 0, 10);
        $currentTime = time();
        if ($currentTime - $unixtime < 60 * $time) {
            return $base64Number;
        } else {
            return $different;
        }
    } else {
        return $different;
    }
}

function checkTimeRedirectOtherWebsite($time, $redirectWebsite)
{   
    if(isset($_GET['n']) && !empty($_GET['n'])){
        if (isset($_GET['u']) && !empty($_GET['u']) ) {
            $unixtime = substr($_GET['u'], 0, 10);
            $currentTime = time();
            // if ($currentTime - $unixtime < 60 * $time) { // time in minute
            if ($currentTime - $unixtime < $time) { // time in second
                return base64_decode($_GET["n"]);
            } else {
                header('Location: ' . $redirectWebsite);
                exit();
            }
        } else {
            // return $base64Number;
            header('Location: ' . $redirectWebsite);
            exit();
        }
    }else{
        header('Location: ' . $redirectWebsite);
        exit();
    }
}


function getRandomNumberFromGroupList($file, $different)
{
    if (isset($_GET['group'])) {
        $data = file_get_contents($file);
        $data = explode("\n", $data);
        $data = array_map('trim', $data);

        $groupList = [];
        $group = "";
        foreach ($data as $key => $value) {

            // if empty, return
            if (empty($value)) continue;

            if (strpos($value, "group") !== false) {
                $group = $value;
            } else {
                $groupList[$group][] = $value;
            }
        }

        $groupName = 'group' . $_GET['group'];
        //check if group is exist
        //if numberList is empty, return different
        if (!isset($groupList[$groupName])) {
            return $different;
        } else {
            $numberList = $groupList[$groupName];
            $phone_number = $numberList[array_rand($numberList)];
            return $phone_number;
        }
    } else {
        return $different;
    }
}

// Get Random Number From Group List
// query string: ?group=1 not found in then show different number
//------------------------------------------------------
// $phone_number = getRandomNumberFromGroupList('http://localhost/public/numbers.txt', "+1-997--000-0000");

// just simple number if you want
// without any condition
//------------------------------------------------------
// $phone_number = isset($_GET["g"]) ? base64_decode($_GET["g"]) : "+1-000--000-0000"; // base64 number if you want replease from query string number

// different number if time is upto 15 minute
// 1s parameter for time in minute,
// 2d parameter for different number if time mismatch 
//------------------------------------------------------
// $phone_number = checkTimeGetNumber(15, "+1-000--000-0000", base64_decode($_GET["g"]));

// redirect to other website if time is upto  15 minute
// 1s parameter for time in minute,
//------------------------------------------------------
// $phone_number = checkTimeRedirectOtherWebsite(3, "https://google.com/", base64_decode($_GET["n"]));
$phone_number = checkTimeRedirectOtherWebsite(15, "https://href.li/?https://totaladblock.com");
